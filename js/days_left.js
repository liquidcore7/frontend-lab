const monthsDurations = Array.from(Array(12).keys()).map(v => new Date(globalTime.getFullYear(), v, 0).getDate());
const quarter = () => Math.trunc(((globalTime.getMonth() + 1) % 12) / 4);

function drawCanvas(ctx, daysLeft, daysTotal) {

    function circleSize(x, y) {return Math.min(x, y) / 2;}
    function calculateBeginPoint(centerx, centery, radius, angle) {
        angle += Math.PI / 2;
        return {x: centerx - radius * Math.sin(angle), y: centery + radius * Math.cos(angle)};
    }

    var drawingContext = ctx.getContext("2d");
    let x = ctx.width - 2, y = ctx.height - 2;
    let daysLeft2Rad = 2 * Math.PI * (daysTotal - daysLeft) / daysTotal - Math.PI / 2;
    let outerCircleSize = circleSize(x, y), innerCircleSize = circleSize(x - 45, y - 45);
    // outerCircleSize == canvas middle

    drawingContext.beginPath();
    drawingContext.arc(outerCircleSize + 1, outerCircleSize, outerCircleSize, 0, 2 * Math.PI);
    drawingContext.strokeStyle = '#d3eaf1';
    drawingContext.stroke();
    drawingContext.closePath();


    let {x: bx, y: by} = calculateBeginPoint(outerCircleSize + 1, outerCircleSize, innerCircleSize, -Math.PI / 2);
    let {x: ex, y: ey} = calculateBeginPoint(outerCircleSize + 1, outerCircleSize, innerCircleSize, daysLeft2Rad);
    let gbGrad = drawingContext.createLinearGradient(bx, by, ex, ey);
    gbGrad.addColorStop(0, '#9fe8d9');
    gbGrad.addColorStop(1, '#53c6ea');


    drawingContext.beginPath();
    drawingContext.arc(outerCircleSize + 1, outerCircleSize, innerCircleSize, -Math.PI / 2, daysLeft2Rad);
    drawingContext.lineWidth = 15;
    drawingContext.strokeStyle = gbGrad;
    drawingContext.stroke();
    drawingContext.closePath();

    drawingContext.beginPath();
    drawingContext.arc(outerCircleSize + 1, outerCircleSize, innerCircleSize, daysLeft2Rad, -Math.PI / 2);
    drawingContext.lineWidth = 15;
    drawingContext.strokeStyle = '#d3eaf1';
    drawingContext.stroke();
    drawingContext.closePath();

    drawingContext.textAlign = "center";
    let bigFontSize = outerCircleSize / 2, smallFontSize = bigFontSize / 4;

    drawingContext.font = "600 " + bigFontSize.toString() + "pt Montserrat";
    drawingContext.fillText(daysLeft.toString(), outerCircleSize + 1, outerCircleSize - smallFontSize + 40);

    drawingContext.font = "300 " + smallFontSize.toString() + "pt Montserrat";
    drawingContext.fillText("days", outerCircleSize + 1, outerCircleSize + smallFontSize + 40);
}

function accumulateMonths(start, end) {
    return monthsDurations.slice(start, end).reduce((a, b) => a + b, 0);
}

function safeMonth() {
    return (globalTime.getMonth() + 1) % 12;
}

function daysLeft() {
    return accumulateMonths(quarter() * 4, quarter() * 4 + 4) -
        (accumulateMonths(quarter() * 4, safeMonth()) + globalTime.getDate());
}

function quarterProgress() {
    return Math.round( (accumulateMonths(quarter() * 4, safeMonth()) + globalTime.getDate()) /
        accumulateMonths(quarter() * 4, quarter() * 4 + 4) * 100);
}

function quarterToString() {
    return `(${months[quarter() * 4]} - ${months[quarter() * 4 + 3]})`
}

function drawUsageCard(q) {
    q.find('.card-title').html('Current quarter ' + quarterToString());

    let thisQuarter = quarter() * 4;
    q.find("#until .small-card-body-font-tiny").html("Until End of " + months[thisQuarter + 3]);
    q.find('progress')[0].value = quarterProgress();

    let [begin, end] = q.find('span.small-card-body-font-tiny');
    begin.textContent = months[thisQuarter].toString() + " 1";
    end.textContent = months[thisQuarter + 3].toString() + " " + monthsDurations[thisQuarter + 3].toString();

    drawCanvas(q.find('#myChart')[0], daysLeft(), accumulateMonths(quarter() * 4, quarter() * 4 + 4));
}



