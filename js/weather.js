const url = "https://api.openweathermap.org/data/2.5/forecast";

async function obtainWeather(header) {
    $.get(
        url,
        {id : 702550, APPID : 'bda799e64e78cd500415dc9ba60e6567'},  // free tier, don`t try to steal it
        function(data) {
                header.find("div .spinner").remove();

                let context = header.find("#temperature");
                let origText = context.html();
                context.html(Math.round(data['list'][0]['main']['temp'] - 273.15) + origText.substring(origText.indexOf('<')));
                context.fadeIn('slow');
        }
    );
}