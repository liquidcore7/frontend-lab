function redrawSlider(ctx) {
    let filledWidth = (ctx.value - ctx.min) / (ctx.max - ctx.min) * 100;
    let gradientCss = `linear-gradient(to right, #53c6ea, #9fe8d9 ${filledWidth}%, #d3eaf1 ${filledWidth}%, #d3eaf1`;
    ctx.style.setProperty('--valuebar-color', gradientCss);
}