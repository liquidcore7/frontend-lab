function hideCard(card) {
    let row = card.closest('.row');
    let children = row.children("div");

    card.parent().fadeOut('slow', function() {
        for (var i = 0; i < children.length; ++i) {
            if (!$(children[i]).is($(this))) {
                children[i].className = children[i].className.split(' ').map(cl => {
                    if (!cl.startsWith("col-")) {
                        return cl;
                    }
                    let position = cl.lastIndexOf('-') + 1;
                    let colNum = parseInt(cl.substring(position));
                    return cl.substring(0, position) + (12 / (12 / colNum - 1)).toString();
                }).join(' ');
            }
        }
    });
}


$(function () {

    let header = $(".modal-header");
    initClock(header);
    obtainWeather(header);
    hourlyCallBacks.push(() => {obtainWeather(header)});

    redrawBalances($("body .container-fluid"));
    redrawSlider($("#brightness")[0]);
    redrawSlider($("#volume")[0]);

    gasChart($("#gas_usage_chart"));
    electricityChart($("#electric_usage_chart"));

    $("#weather-enabled").click(() => {
        $("#weather-container").fadeToggle(300);
    });

    $(".message :button").click(function() {$(this).closest('div .message').slideUp();});

    let q = $("#quarter");
    drawUsageCard(q);

    dailyCallBacks.push(() => drawUsageCard(q));

    $(".card-closer").click(function() {hideCard($(this).closest('div .card'))});
    $(".currency-selector").click(function() {
        let clickedBtn = $(this);
        currentCurrency = clickedBtn.html();
        reassignButtons(clickedBtn);
        redrawBalances($("body .container-fluid"));
    });

});


