function drawBalancePointer(ctx, balance) {

    let maxMoney = 500;
    let dataArcsBegin = 81 * Math.PI / 120;
    let computedBalance =  2 * (Math.PI - dataArcsBegin) * balance / maxMoney;
    let size = Math.min(ctx.width, ctx.height),
        center = size / 2;

    let initialAlpha = 0.585 * Math.PI;
    let drawingContext = ctx.getContext('2d');

    function drawRotated(image, sz, degrees) {
        drawingContext.save();
        drawingContext.translate(ctx.width / 2, ctx.height / 2);
        drawingContext.rotate(-degrees);
        drawingContext.drawImage(image, -sz / 2, -sz / 2, sz, sz);
        drawingContext.restore();
    }

    function drawText() {
        let bigFontSize = size / 14, smallFontSize = bigFontSize / 1.5;
        let [sign, value] = moneyToString(balance).split(' ');

        drawingContext.font = "400 " + smallFontSize.toString() + "pt Montserrat";
        let smallTextWidth = drawingContext.measureText(sign + " ").width;
        drawingContext.font = "700 " + bigFontSize.toString() + "pt Montserrat";
        let largeTextWidth = drawingContext.measureText(value).width;

        let totalWidth = smallTextWidth + largeTextWidth;
        let heightDelta = bigFontSize - smallFontSize;

        drawingContext.font = "400 " + smallFontSize.toString() + "pt Montserrat";
        drawingContext.fillText(sign, center - totalWidth / 2, center);

        drawingContext.font = "700 " + bigFontSize.toString() + "pt Montserrat";
        drawingContext.fillText(value, center - totalWidth / 2 + smallTextWidth, center + heightDelta / 2);
    }

    var pointerSprite = new Image();
    pointerSprite.src = 'img/svg/balance_arrow.svg';
    pointerSprite.onload = () => {
        drawRotated(pointerSprite, size / 2, initialAlpha - computedBalance);
        drawingContext.fillStyle = '#fff';
        drawText();
    };
}


function drawBalanceBar(ctx, balance, prevBalance) {
    let maxMoney = 500;

    var drawingContext = ctx.getContext('2d');
    let center = Math.min(ctx.width, ctx.height) / 2;
    let startingAngle = 2 * Math.PI / 3;



    function calculateBeginPoint(center, radius, angle) {
        angle -= Math.PI / 2;
        return {x: center - radius * Math.sin(angle), y: center + radius * Math.cos(angle)};
    }

    function calculateBezierCurveControlPoint(center, radius, angle, anticlockwise) {
        let df = anticlockwise ? -Math.PI / 40 : Math.PI / 40;
        return {
            cp: calculateBeginPoint(center, radius, angle + df),
            orig: calculateBeginPoint(center, radius, angle)
        }
    }

    function drawArc(center, radius, startingAngle, endingAngle, style, fill = true, antcl = false) {
        if (!fill)
            drawingContext.beginPath();

        drawingContext.arc(center, center, radius, startingAngle, endingAngle, antcl);

        drawingContext.strokeStyle = style;
        drawingContext.stroke();

        if (!fill)
            drawingContext.closePath();
    }

    function drawBezierEdge(center, radiusI, radiusO, angle, style, withFill = false, anticlockwise = false) {
        let inner = calculateBezierCurveControlPoint(center, radiusI, angle, anticlockwise),
            outer = calculateBezierCurveControlPoint(center, radiusO, angle, anticlockwise);

        if (!withFill) {
            drawingContext.beginPath();
            drawingContext.moveTo(inner.orig.x, inner.orig.y);
        }

        drawingContext.bezierCurveTo(inner.cp.x, inner.cp.y, outer.cp.x, outer.cp.y, outer.orig.x, outer.orig.y);
        drawingContext.strokeStyle = style;
        drawingContext.stroke();

        if (!withFill)
            drawingContext.closePath();

    }

    function drawCurvedArc(center, radius, startingAngle, endingAngle, style, lineWeight, withFill) {
        if (withFill)
            drawingContext.beginPath();

        drawArc(center, radius - lineWeight, startingAngle, endingAngle, style, withFill);
        drawBezierEdge(center, radius - lineWeight, radius, endingAngle, style, withFill);
        drawArc(center, radius, endingAngle, startingAngle, style, withFill, true);
        drawBezierEdge(center, radius, radius - lineWeight, startingAngle, style, withFill, true);

        if (withFill) {
            drawingContext.closePath();
            drawingContext.fillStyle = style;
            drawingContext.fill();
        }
    }

    function drawHeadingCircle(center, radius, endAngle, diameter) {
        let {x: centerX, y: centerY} = calculateBeginPoint(center, radius, endAngle - Math.PI / 240);

        drawingContext.beginPath();
        drawingContext.fillStyle = '#fff';
        drawingContext.arc(centerX, centerY, diameter / 2, 0, Math.PI * 2);
        drawingContext.fill();
        drawingContext.closePath();
    }


    drawCurvedArc(center, center - 5, startingAngle, startingAngle - Math.PI / 3, '#d3eaf1', 32, false);
    let dataArcsBegin = startingAngle + Math.PI / 120;
    let computedPrevBalance = dataArcsBegin + 2 * (Math.PI - dataArcsBegin) * prevBalance / maxMoney,
        computedBalance = dataArcsBegin + 2 * (Math.PI - dataArcsBegin) * balance / maxMoney;

    drawCurvedArc(center, center - 10, dataArcsBegin, computedPrevBalance, '#d3eaf1', 22, true);

    let {x: bx, y: by} = calculateBeginPoint(center, center - 21, dataArcsBegin);
    let {x: ex, y: ey} = calculateBeginPoint(center, center - 21, computedBalance);
    let gbGrad = drawingContext.createLinearGradient(bx, by, ex, ey);
    gbGrad.addColorStop(0, '#9fe8d9');
    gbGrad.addColorStop(1, '#53c6ea');

    drawCurvedArc(center, center - 10, dataArcsBegin, computedBalance, gbGrad, 22, true);
    drawHeadingCircle(center, center - 21, computedBalance, 14);
}


function drawBalance(ctx, balance, prevBalance) {
    drawBalanceBar(ctx, balance, prevBalance);
    drawBalancePointer(ctx, balance);
}
