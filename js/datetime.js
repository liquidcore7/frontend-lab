var globalTime = new Date;
let beginHours = globalTime.getHours(), beginMinutes = globalTime.getMinutes(), msAlpha = 60 * 1000 - globalTime.getMilliseconds() - globalTime.getSeconds() * 1000;

const daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const months = ["Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov"];

function getMonth(idx) {
    return months[(idx + 1) % 12];
}

var hourlyCallBacks = [];
var dailyCallBacks = [];

var time = {
    hours: beginHours,
    minutes: beginMinutes,
    ampm: (beginHours > 12) ? "PM" : "AM"
};

time.hours %= 12;

function itos(i) {
    return ((i < 10) ? "0" + i.toString() : i.toString());
}

function updateClock(header) {
    let clockElement = header.find("#clock");
    let prevText = clockElement.html();
    clockElement.html(((time.hours === 0) ? "12" : itos(time.hours)) + ":" + itos(time.minutes) + prevText.substring(prevText.indexOf('<')));
    clockElement.find("#ampm").html(time.ampm);

    ++time.minutes;
    if (time.minutes >= 60) {
        time.hours += time.minutes / 60;
        time.minutes %= 60;
        hourlyCallBacks.forEach(fn => fn());
    }
    if (time.hours >= 12) {
        time.hours %= 12;
        let midnightChange = time.ampm === "PM";
        time.ampm = (midnightChange) ? "AM" : "PM";
        if (midnightChange) {
            dailyCallBacks.forEach(fn => fn());
        }
    }

    if (time.hours === 0 && time.minutes === 0)
        updateCalendar(header);
}

function updateCalendar(header) {
    globalTime = new Date;
    header.find(".day").html(daysOfWeek[globalTime.getDay()]);
    header.find(".date").html(getMonth(globalTime.getMonth()) + " " + globalTime.getDate().toString())
}

function initClock(header)
{
    updateClock(header);
    updateCalendar(header);
    setTimeout(() => {updateClock(header); updateCalendar(header); clockRoutine(header)}, msAlpha);
    dailyCallBacks.push(() => updateCalendar(header));
}

function clockRoutine(header) {
    setInterval(() => updateClock(header), 60 * 1000);
}




