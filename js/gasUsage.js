const blue = "rgba(3, 58, 255, 0.4)",
    lightBlue = "rgba(73, 118, 215, 0.02)";

const gasData = {
    actual: [100, 110, 270, 230, 210, 230, 280, 300, 220, 160, 200, 20],
    old: [80, 160, 240, 300, 250, 270, 290, 270, 270, 240, 210, 180, 120]
};

const electricityData = {
    actual: [140, 135, 130, 125, 120, 150, 150, 140, 150, 120, 300, 300],
    old: [140, 170, 170, 180, 170, 190, 150, 170, 180, 170, 180, 185]
};

function data(ctx, dataset1, dataset2) {
    let drawingCtx = ctx[0].getContext('2d');
    let grad = drawingCtx.createLinearGradient(0, ctx.height(), 0, ctx.height() / 8);
    grad.addColorStop(0, lightBlue);
    grad.addColorStop(1, blue);

    return {
        labels: months,
        datasets: [
            {
                lineTension: 0,
                label: "Current month",
                backgroundColor: grad,
                borderColor: blue.substring(0, blue.lastIndexOf(',')) + ", 0.5)",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: blue.substring(0, blue.lastIndexOf(',')) + ", 1)",
                pointHoverBorderWidth: 7,
                pointHoverRadius : 10,
                pointRadius: 0,
                pointHitRadius: 20,
                data: dataset1
            },
            {
                lineTension: 0,
                label: "Past month",
                backgroundColor: grad,
                borderColor: lightBlue.substring(0, lightBlue.lastIndexOf(',')) + ", 0.5)",
                pointHoverRadius : 0,
                pointRadius: 0,
                data: dataset2
            }
        ]
    };
}

const toolTipCss = "linear-gradient(to bottom, rgba(255, 255, 255, 1), rgba(255, 255, 255, 0))";
const toolTipHtml = "<div class='col' style='margin: {0}px 5px 5px 0'>" +
    "<div class='small-card-body-font' style='text-align: center'>{1}</div><div class='small-card-body-font' style='text-align: center'>{2}</div></div>";

function getTooltipGenerator(units) {
    return function(tooltipModel) {
        // Tooltip Element
        var tooltipEl = document.getElementById('chartjs-tooltip');

        // Create element on first render
        if (!tooltipEl) {
            tooltipEl = document.createElement('div');
            tooltipEl.id = 'chartjs-tooltip';
            tooltipEl.style.backgroundImage = toolTipCss;
            tooltipEl.style.height = this._chart.canvas.height + 'px';
            tooltipEl.style.width = "120px";
            tooltipEl.innerHTML = toolTipHtml;
            document.body.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltipModel.opacity === 0) {
            tooltipEl.style.opacity = "0";
            return;
        }

        function strfmt(format) {
            var args = Array.prototype.slice.call(arguments, 1);
            return format.replace(/{(\d+)}/g, function(match, number) {
                return typeof args[number] != 'undefined'
                    ? args[number]
                    : match
                    ;
            });
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltipModel.yAlign) {
            tooltipEl.classList.add(tooltipModel.yAlign);
        } else {
            tooltipEl.classList.add('no-transform');
        }

        // Set Text
        let yValue = 304 - parseInt(tooltipModel.caretY) * 0.73;
        tooltipEl.innerHTML = strfmt(toolTipHtml, tooltipModel.caretY, Math.round(yValue * 0.7) + " " + units, moneyToString(yValue));

        // `this` will be the overall tooltip
        let position = this._chart.canvas.getBoundingClientRect();

        // Display, position, and set styles for font
        tooltipEl.style.opacity = "1";
        tooltipEl.style.position = 'absolute';
        tooltipEl.style.left = position.left + tooltipModel.caretX + 10 + 'px';
        tooltipEl.style.top = (position.top + 40) + 'px';
        tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
        tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
        tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
    };
}



function gasChart(ctx) {
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data(ctx, gasData.actual, gasData.old),
        options: { legend: {display: false}, tooltips: {enabled: false, custom: getTooltipGenerator("litres")}, scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }}
    });
}

function electricityChart(ctx) {
    var myChart = new Chart(ctx, {
        type: 'line',
        data: data(ctx, electricityData.actual, electricityData.old),
        options: { legend: {display: false}, tooltips: {enabled: false, custom: getTooltipGenerator("kWh")}, scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }}
    })

}