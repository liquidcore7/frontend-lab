const conversions = {
    "EUR": {multiplier: 1.154735, sign: "€"},
    "USD": {multiplier: 1.0, sign: "$"},
    "UAH": {multiplier: 0.035815, sign: "₴"}
};

var currentCurrency = "USD";
var currentBalance = 340;

let balance = {
  thisQuarter: 6500,
  quarterIncome: 250,
  gasUsage: 95,
  electricUsage: 67
};

function moneyToString(amount) {
    let conversion = conversions[currentCurrency];
    return conversion.sign + " " + Math.round(amount / conversion.multiplier);
}

function redrawBalances(context) {
    Object.getOwnPropertyNames(balance).forEach((name, _, __) => {
        context.find("#" + name).html(moneyToString(balance[name]));
    });
    drawBalance(context.find("#balanceChart")[0], currentBalance, 710);
}

function reassignButtons(btnContext) {
    let parentCtx = btnContext.closest(".container-fluid");
    parentCtx.find(".dropdown-toggle").html(currentCurrency);
    let alternateCurrencies = Object.getOwnPropertyNames(conversions).filter((name) => name !== currentCurrency);
    let childButtons = parentCtx.find(".dropdown-item");
    for (var i = 0; i < childButtons.length; ++i) {
        childButtons[i].innerHTML = alternateCurrencies[i];
    }
}